package com.exxeta.bibleschedule;

import org.joda.time.format.DateTimeFormat;
import org.junit.Test;

import java.time.YearMonth;
import java.time.format.DateTimeFormatter;

import static org.junit.Assert.assertEquals;

public class MainActivityTest {

    @Test
    public void testMonthYearFormatter() {
        DateTimeFormatter monthYearFormatter = DateTimeFormatter.ofPattern("MMMM yyyy");
        YearMonth thisMonth = YearMonth.now();

        org.joda.time.format.DateTimeFormatter monthYearFmtOlder = DateTimeFormat.forPattern("MMMM yyyy");
        org.joda.time.YearMonth thisMonthOlder = org.joda.time.YearMonth.now();

        String firstText = thisMonth.format(monthYearFormatter);
        String secondsText = thisMonthOlder.toString(monthYearFmtOlder);

        assertEquals(firstText, secondsText);
    }

}