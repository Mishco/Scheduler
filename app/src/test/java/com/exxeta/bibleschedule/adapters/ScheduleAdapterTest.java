package com.exxeta.bibleschedule.adapters;

import org.junit.Test;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import static org.junit.Assert.assertEquals;

public class ScheduleAdapterTest {

    @Test
    public void testOnLongClick() {
        Date date = Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant());
        Date dateOld = org.joda.time.LocalDate.now().toDateTimeAtStartOfDay().toDate();

        assertEquals(date, dateOld);
    }
}