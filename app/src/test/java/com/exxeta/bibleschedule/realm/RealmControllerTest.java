package com.exxeta.bibleschedule.realm;

import org.joda.time.Interval;
import org.joda.time.YearMonth;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Test;

import java.time.ZoneId;
import java.util.Date;

import static org.junit.Assert.assertNotNull;

public class RealmControllerTest {

    @Test
    public void getScheduleFromMonth() {
        // Mockito TODO
        //RealmController realmController = new RealmController();
        java.time.YearMonth actMonth = java.time.YearMonth.now();

        Date dateFrom = Date.from(actMonth.atDay(1).atStartOfDay(ZoneId.systemDefault()).toInstant());
        Date dateTo = Date.from(actMonth.atEndOfMonth().atStartOfDay(ZoneId.systemDefault()).toInstant());
        System.out.println(dateFrom);
        System.out.println(dateTo);
    }

    @Test
    public void getScheduleFromMonthOlder() {
        org.joda.time.YearMonth thisMonthOlder = YearMonth.now();

        Date dateFrom = thisMonthOlder.toLocalDate(1).toDate();
        Date dateTo = thisMonthOlder.toLocalDate(31).toDate();
        System.out.println(dateFrom);
        System.out.println(dateTo);
        assertNotNull(dateFrom);
        assertNotNull(dateTo);
    }

    @Test
    public void testStartOfMonth() {
        DateTimeFormatter fmt = DateTimeFormat.forPattern("dd.MM.yyyy");
        org.joda.time.YearMonth thisMonthOlder = YearMonth.parse("1.1.2019", fmt);

        Date dateFrom = thisMonthOlder.toLocalDate(1).toDate();
        assertNotNull(dateFrom);

        // special month
        thisMonthOlder = YearMonth.parse("10.10.2010", fmt);
        dateFrom = thisMonthOlder.toLocalDate(1).toDate();
        assertNotNull(dateFrom);
    }

    @Test
    public void testEndOfMonth() {
        DateTimeFormatter fmt = DateTimeFormat.forPattern("dd.MM.yyyy");
        // February
        org.joda.time.YearMonth thisMonthOlder = YearMonth.parse("1.2.2019", fmt);

        Interval interval = thisMonthOlder.toInterval();
        System.out.println(interval.getStart());            // start of the month
        System.out.println(interval.getEnd().minusDays(1)); // end of the month

        Date dateTo = interval.getEnd().minusDays(1).toDate();

        assertNotNull(dateTo);
    }

}