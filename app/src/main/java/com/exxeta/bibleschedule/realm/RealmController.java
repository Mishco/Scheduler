package com.exxeta.bibleschedule.realm;

import android.app.Activity;
import android.app.Application;
import android.support.v4.app.Fragment;

import com.exxeta.bibleschedule.model.Schedule;

import org.joda.time.Interval;

import java.time.YearMonth;
import java.time.ZoneId;
import java.util.Date;

import io.realm.Realm;
import io.realm.RealmResults;

public class RealmController {
    private static RealmController instance;
    private final Realm realm;

    public RealmController(Application application) {
        realm = Realm.getDefaultInstance();
    }

    public static RealmController with(Fragment fragment) {
        if (instance == null) {
            instance = new RealmController(fragment.getActivity().getApplication());
        }
        return instance;
    }

    public static RealmController with(Activity activity) {
        if (instance == null) {
            instance = new RealmController(activity.getApplication());
        }
        return instance;
    }

    public static RealmController with(Application application) {
        if (instance == null) {
            instance = new RealmController(application);
        }
        return instance;
    }

    public static RealmController getInstance() {
        return instance;
    }

    public Realm getRealm() {
        return realm;
    }

    //Refresh the realm istance
    public void refresh() {
        realm.refresh();
    }

    //clear all objects from Schedule.class
    public void clearAll() {
        realm.beginTransaction();
        realm.deleteAll();
        realm.commitTransaction();
    }

    //find all objects in the Book.class
    public RealmResults<Schedule> getBooks() {
        return realm.where(Schedule.class).findAll();
    }


    public RealmResults<Schedule> getScheduleFromMonth(YearMonth actMonth) {
        Date dateFrom = null;
        Date dateTo = null;


        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            // TODO check in JANUARY, which date is picked on android
            dateFrom = Date.from(actMonth.atDay(1).minusDays(2).atStartOfDay(ZoneId.systemDefault()).toInstant());
            dateTo = Date.from(actMonth.atEndOfMonth().plusDays(1).atStartOfDay(ZoneId.systemDefault()).toInstant());
        }
        if (dateFrom != null && dateTo != null) {
            return realm.where(Schedule.class).between("date", dateFrom, dateTo).findAll();
        } else {
            return null;
        }
    }

    private RealmResults<Schedule> getJanuaryDate() {
        Date startMonth = new Date("01/01/2019");
        Date endMonth = new Date("31/01/2019");
        return realm.where(Schedule.class).greaterThanOrEqualTo("date", startMonth).and().lessThanOrEqualTo("date", endMonth);
    }

    public RealmResults<Schedule> getScheduleFromMonthOlder(org.joda.time.YearMonth thisMonthOlder) {
        Interval interval = thisMonthOlder.toInterval();
        Date dateFrom = thisMonthOlder.toLocalDate(1).toDate();
        Date dateTo = interval.getEnd().minusDays(1).toDate();

        return realm.where(Schedule.class).between("date", dateFrom, dateTo).findAll();
    }

    //query a single item with the given id
    public Schedule getBook(String id) {
        return realm.where(Schedule.class).equalTo("id", id).findFirst();
    }

    //check if Book.class is empty
    public boolean hasBooks() {
        return !realm.where(Schedule.class).findAll().isEmpty();
    }

}
